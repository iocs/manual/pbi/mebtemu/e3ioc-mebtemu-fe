require stream
require essioc
require xtpico
require fug

errlogInit(20000)

#- 1 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "1000000")

#- ESSIOC configurations
epicsEnvSet("LOG_SERVER_NAME", "172.16.107.59")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("FRONTEND_IP", "172.16.60.108")
epicsEnvSet("HMC804x_IP",  "172.16.62.20")
epicsEnvSet("FUG_IP",      "172.16.62.0")

# streamDevice protocol files search path
epicsEnvSet("STREAM_PROTOCOL_PATH", "$(E3_CMD_TOP)/hmc804x:$(fug_DB)")

# XTpico

#- Device configurations
epicsEnvSet("LOCATION", "MEBT-010")
epicsEnvSet("DEVICE_NAME", "PBI-EMUFE-001")
epicsEnvSet("DEVICE_IP", "$(FRONTEND_IP)")

#- Variables settings
epicsEnvSet("PREFIX", "$(LOCATION):$(DEVICE_NAME):")
#- asyn IP comm ports
epicsEnvSet("I2C_COMM_PORT","I2C_COMM_$(DEVICE_NAME)")
epicsEnvSet("SPI_COMM_PORT","SPI_COMM_$(DEVICE_NAME)")

#- Create the asyn port to talk XTpico PORT1; TCP port 1002.
drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):1002")
#- asynSetTraceIOMask("$(I2C_COMM_PORT)",0,255)
#- asynSetTraceMask("$(I2C_COMM_PORT)",0,255)

#- Create the asyn port to talk XTpico PORT2; TCP port 1003.
#- drvAsynIPPortConfigure($(SPI_COMM_PORT),"$(DEVICE_IP):1003")
#- asynSetTraceIOMask("$(SPI_COMM_PORT)",0,255)
#- asynSetTraceMask("$(SPI_COMM_PORT)",0,255)

#- seven TMP100 sensors
iocshLoad("$(E3_CMD_TOP)/iocsh/tmp100.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1_$(DEVICE_NAME), NAME=TMP100, COUNT=7, INFOS=0x49; 0x4A; 0x4B; 0x4C; 0x4D; 0x4E; 0x4F")

#- EEPROM
iocshLoad("$(E3_CMD_TOP)/iocsh/m24m02.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1_$(DEVICE_NAME), NAME=Eeprom, COUNT=1, INFOS=0x50")

#- one port expander
iocshLoad("$(E3_CMD_TOP)/iocsh/tca9555.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1_$(DEVICE_NAME), NAME=IO1, COUNT=1, INFOS=0x21")

#- one voltage monitor
iocshLoad("$(E3_CMD_TOP)/iocsh/ltc2991.iocsh", "IP_PORT=$(I2C_COMM_PORT), N=1_$(DEVICE_NAME), NAME=VM1, COUNT=1, INFOS=0x48")

# HMC804x

epicsEnvSet("ASYN_PORT", "HMC804x-asyn-port")
epicsEnvSet("DEVICE_IP", "$(HMC804x_IP)")
epicsEnvSet("P", "PBI-EMU02:")
epicsEnvSet("R", "PwrC-PSU-002:")
drvAsynIPPortConfigure("${ASYN_PORT}", "${DEVICE_IP}:5025")

## Load record instances
dbLoadRecords("$(E3_CMD_TOP)/hmc804x/hmc804x.template", "PORT=${ASYN_PORT},P=${P},R=${R=}")
dbLoadRecords("$(E3_CMD_TOP)/hmc804x/channel.template", "PORT=${ASYN_PORT},P=${P},R=${R=},CH=1")
dbLoadRecords("$(E3_CMD_TOP)/hmc804x/channel.template", "PORT=${ASYN_PORT},P=${P},R=${R=},CH=2")
dbLoadRecords("$(E3_CMD_TOP)/hmc804x/channel.template", "PORT=${ASYN_PORT},P=${P},R=${R=},CH=3")

# FUG Power Supply
drvAsynIPPortConfigure("FUG_HVPS", "$(FUG_IP):2101")
dbLoadRecords("$(fug_DB)/fug_HV.template",    "proto_file=fug_HV.proto, Ch_name=FUG_HVPS, P=PBI-EMU02:, R=PwrC-PSU-001:, EGU=kV, ASLO=0.1")

# Top-level manager
epicsEnvSet("LOCATION", "MEBT-010")
epicsEnvSet("DEVICE_NAME", "PBI-EMUFE-002")
epicsEnvSet("PREFIX", "$(LOCATION):$(DEVICE_NAME)")

dbLoadRecords("$(E3_CMD_TOP)/db/manager.db", "P=${PREFIX}, P_XTPICO=MEBT-010:PBI-EMUFE-001, P_DAQ_1=PBI-EMU02:Ctrl-AMC-110:, P_DAQ_2=PBI-EMU02:Ctrl-AMC-120:")

iocInit()
